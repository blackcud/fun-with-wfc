class TimingEvent:
    def __init__(self, timeout: float):
        """
        Create a new timer
        :param timeout: interval in seconds
        """
        if timeout <= 0:
            raise ValueError('timeout has to be positive')
        self.timeout = timeout
        self._time_remaining = timeout
        self.fire_counter = 0

    def _update(self, time_delta: float) -> None:
        self._time_remaining -= time_delta

    def should_fire(self) -> bool:
        """
        Returns true if the timer is overdue/ready
        :return:
        """
        return self._time_remaining <= 0.

    def get_remaining_time(self) -> float:
        """
        Returns the time until it fires next. The return value is clamped to [0,timeout].
        :return: Time until fire in seconds. Min: 0. Max: timeout.
        """
        if self.should_fire():
            return 0.
        elif self._time_remaining > self.timeout:
            return self.timeout
        else:
            return self._time_remaining

    def get_progress_scaler(self):
        return 1 - self.get_remaining_time() / self.timeout

    def get_progress_scaler_remaining(self):
        return self.get_remaining_time() / self.timeout

    def update_and_fire_check(self, time_delta: float) -> bool:
        """
        Returns true if the update pushed the Timer into threshold.
        This method should be run on every game update cycle.
        :param time_delta:
        :return:
        """
        self._update(time_delta)
        if self.should_fire():
            self._time_remaining += self.timeout
            self.fire_counter += 1
            return True
        else:
            return False

    def reset(self, reset_fire_counter: bool = False, new_timeout: float = None) -> None:
        """
        Reset timer to starting value
        :param new_timeout: Use this new timeout for resetting
        :param reset_fire_counter: Optionally reset fire counter
        :return:
        """
        if new_timeout:
            self.timeout = new_timeout
        self._time_remaining = self.timeout
        if reset_fire_counter:
            self.fire_counter = 0

    def change_timeout(self, new_timeout: float, keep_progress: bool = True):
        """
        Sets the timeout to the new value and remaining time to a value that corresponds to the percentage
        :param keep_progress: use remaining percentage
        :param new_timeout: the new timeout value
        :return:
        """
        if new_timeout <= 0:
            raise ValueError('new_timeout has to be positive')
        if keep_progress:
            self._time_remaining = new_timeout * self.get_progress_scaler_remaining()
        else:
            self._time_remaining = new_timeout
        self.timeout = new_timeout
