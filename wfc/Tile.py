from __future__ import annotations

from PIL import Image


class Tile:
    sides = ['north', 'east', 'south', 'west']

    def __init__(self, image_path: str, sockets_per_edge: int = 3, generate_sockets: bool = True):
        print('Creating new Tile from', image_path)
        self.sockets_per_edge = sockets_per_edge
        self.sockets = {}
        self.image_path = image_path
        self.pil_image = Image.open(image_path)
        self.pixel_image = self.pil_image.load()
        self.width, self.height = self.pil_image.size
        assert self.width == self.height, "Please use quadratic images"

        if generate_sockets:
            self.generate_sockets()

    def get_color_of_pixel(self, x, y) -> tuple[int, int, int]:
        return self.pixel_image[x, y]

    def generate_sockets(self) -> None:
        """
        Takes the color of pixels along each edge of the image and saves them to self.sockets dict. The sockets are distributed evenly along each of the four edges based on sockets_per_edge
        """
        north_sockets = {}
        east_sockets = {}
        south_sockets = {}
        west_sockets = {}

        # we add all positions clockwise, assuming 0:0 coordinates are in the top-left
        # north: left to right
        for index, pos in enumerate(range(0, self.width, self.width // (self.sockets_per_edge + 1))[1:]):
            north_sockets[index] = self.get_color_of_pixel(pos, 0)
        # east: top to bottom
        for index, pos in enumerate(range(0, self.height, self.height // (self.sockets_per_edge + 1))[1:]):
            east_sockets[index] = self.get_color_of_pixel(self.width - 1, pos)
        # south: right to left
        for index, pos in enumerate(range(self.width, 0, self.width // (self.sockets_per_edge + 1) * -1)[1:]):
            south_sockets[index] = self.get_color_of_pixel(pos, self.height - 1)
        # west: bottom to top
        for index, pos in enumerate(range(self.height, 0, self.height // (self.sockets_per_edge + 1) * -1)[1:]):
            west_sockets[index] = self.get_color_of_pixel(0, pos)

        self.sockets['north'] = north_sockets
        self.sockets['east'] = east_sockets
        self.sockets['south'] = south_sockets
        self.sockets['west'] = west_sockets

    def is_socket_fit(self, incoming_socket_dict: dict, side: str) -> bool:
        # TODO: make this a class function?
        """
        Compares the provided edge sockets of this image with the incoming sockets in reverse order
        :param side: north, east, south, west
        :param incoming_socket_dict: the corresponding
        :return:
        """
        for my_socket_key, my_socket_value in self.sockets[side].items():
            if self.sockets[side][my_socket_key] != incoming_socket_dict[self.sockets_per_edge - my_socket_key - 1]:
                return False
        return True

    def create_rotational_copy(self) -> Tile:
        """
        Returns a copy of this tile rotate 90° clockwise.
        :return:
        """
        r_tile = Tile(image_path=self.image_path, sockets_per_edge=self.sockets_per_edge, generate_sockets=False)

        r_tile.sockets['east'] = self.sockets['north'].copy()
        r_tile.sockets['south'] = self.sockets['east'].copy()
        r_tile.sockets['west'] = self.sockets['south'].copy()
        r_tile.sockets['north'] = self.sockets['west'].copy()

        return r_tile

    def __repr__(self):
        return 'Tile:' + self.image_path.split('\\')[-1] + f'({self.width}x{self.height}) {self.sockets_per_edge}'
