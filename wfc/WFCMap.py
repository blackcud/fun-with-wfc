import random
from operator import itemgetter
from pprint import pprint


class WFCMap:
    def __init__(self, width: int, height: int, variations: list, init_grid: bool = True):
        """
        A data structure to represent the entropy values on a grid and perform some helper methods on these values.
        :param width:
        :param height:
        :param variations:
        """
        self.width = width
        self.height = height
        self.grid = None
        self.resolved = None
        self.variations = variations

        if init_grid:
            self.init_grid()

    def init_grid(self):
        self.grid = [[0] * self.width for i in range(self.height)]
        for x in range(0, self.width):
            for y in range(0, self.height):
                self.grid[x][y] = self.variations.copy()

    def get_entropy(self, x, y):
        """
        Returns the number of remaining variations at this cell in the list
        :param x:
        :param y:
        :return:
        """
        if type(self.grid[x][y]) is list:
            return len(self.grid[x][y])
        else:
            return 1

    def resolve_cell_randomly(self, x, y) -> bool:
        """
        Resolved this cell in the grid randomly
        :param x:
        :param y:
        :return: optional bool return value. true if change to grid occurred.
        """
        if self.get_entropy(x, y) > 1:
            v = random.choice(self.grid[x][y])
            print(f'Resolving cell randomly: {x}/{y} to {v}')
            self.grid[x][y] = v
            self.reduce_variations_north_of_position(x, y)
            return True
        else:
            return False

    def reduce_variations_north_of_position(self, x, y) -> bool:
        """
        Attempts to reduce the amount of variations in the cell north of this one
        :param x:
        :param y:
        :return: optional return value indicating change to the grid
        """
        check_x = x
        check_y = y - 1
        if check_y < 0:  # Nothing to check if we leave the grid
            return False
        if self.is_resolved(check_x, check_y):  # Nothing to do since we are done
            return False

        variations_to_check = self.grid[check_x][check_y]
        new_variations = set()
        for other in variations_to_check:
            if self.is_resolved(x, y):
                var = self.grid[x][y]
                if var.is_socket_fit(side='north', incoming_socket_dict=other.sockets['south']):
                    new_variations.add(other)
            else:
                for var in self.grid[x][y]:
                    if var.is_socket_fit(side='north', incoming_socket_dict=other.sockets['south']):
                        new_variations.add(other)
        return self._check_in_new_variations(check_x, check_y, new_variations, variations_to_check)

    def reduce_variations_south_of_position(self, x, y) -> bool:
        """
        Attempts to reduce the amount of variations in the cell south of this one
        :param x:
        :param y:
        :return: optional return value indicating change to the grid
        """
        check_x = x
        check_y = y + 1
        if check_y >= self.height:  # Nothing to check if we leave the grid
            return False
        if self.is_resolved(check_x, check_y):  # Nothing to do since we are done
            return False

        variations_to_check = self.grid[check_x][check_y]
        new_variations = set()
        for other in variations_to_check:
            if self.is_resolved(x, y):
                var = self.grid[x][y]
                if var.is_socket_fit(side='south', incoming_socket_dict=other.sockets['north']):
                    new_variations.add(other)
            else:
                for var in self.grid[x][y]:
                    if var.is_socket_fit(side='south', incoming_socket_dict=other.sockets['north']):
                        new_variations.add(other)
        return self._check_in_new_variations(check_x, check_y, new_variations, variations_to_check)

    def reduce_variations_east_of_position(self, x, y) -> bool:
        """
        Attempts to reduce the amount of variations in the cell east of this one
        :param x:
        :param y:
        :return: optional return value indicating change to the grid
        """
        check_x = x + 1
        check_y = y
        if check_x >= self.width:  # Nothing to check if we leave the grid
            return False
        if self.is_resolved(check_x, check_y):  # Nothing to do since we are done
            return False

        variations_to_check = self.grid[check_x][check_y]
        new_variations = set()
        for other in variations_to_check:
            if self.is_resolved(x, y):
                var = self.grid[x][y]
                if var.is_socket_fit(side='east', incoming_socket_dict=other.sockets['west']):
                    new_variations.add(other)
            else:
                for var in self.grid[x][y]:
                    if var.is_socket_fit(side='east', incoming_socket_dict=other.sockets['west']):
                        new_variations.add(other)
        return self._check_in_new_variations(check_x, check_y, new_variations, variations_to_check)

    def reduce_variations_west_of_position(self, x, y) -> bool:
        """
        Attempts to reduce the amount of variations in the cell west of this one
        :param x:
        :param y:
        :return: optional return value indicating change to the grid
        """
        check_x = x - 1
        check_y = y
        if check_x < 0:  # Nothing to check if we leave the grid
            return False
        if self.is_resolved(check_x, check_y):  # Nothing to do since we are done
            return False

        variations_to_check = self.grid[check_x][check_y]
        new_variations = set()
        for other in variations_to_check:
            if self.is_resolved(x, y):
                var = self.grid[x][y]
                if var.is_socket_fit(side='west', incoming_socket_dict=other.sockets['east']):
                    new_variations.add(other)
            else:
                for var in self.grid[x][y]:
                    if var.is_socket_fit(side='west', incoming_socket_dict=other.sockets['east']):
                        new_variations.add(other)
        return self._check_in_new_variations(check_x, check_y, new_variations, variations_to_check)

    def _check_in_new_variations(self, check_x, check_y, new_variations: set, variations_to_check) -> bool:
        # TODO: remake this code nicer
        if len(new_variations) == 1:
            self.grid[check_x][check_y] = list(new_variations)[0]
        else:
            self.grid[check_x][check_y] = list(new_variations)
        return len(variations_to_check) != len(new_variations)

    def get_number_of_resolved_cells(self) -> int:
        num = 0
        for x in range(self.width):
            for y in range(self.width):
                if self.is_resolved(x, y) == 1:
                    num += 1
        return num

    def reduce_surrounding_cells(self, x, y) -> bool:
        """
        Try to reduce orthogonally adjacent cells' variations.
        :param x:
        :param y:
        :return: Optional return value True if something in a neighbouring cell has changed
        """
        changed = False
        if self.reduce_variations_north_of_position(x, y):
            changed = True
        if self.reduce_variations_east_of_position(x, y):
            changed = True
        if self.reduce_variations_south_of_position(x, y):
            changed = True
        if self.reduce_variations_west_of_position(x, y):
            changed = True
        return changed

    def reduce_map(self, recursive=True):
        """
        This method is not optimized and should be avoided later on
        :return:
        """
        print('starting reduce map')
        total_entropy_old = self.get_total_entropy()
        for x in range(self.width):
            for y in range(self.height):
                if self.get_entropy(x, y) > 1 and self.get_surrounding_sum(x, y) < self.num_variations() * 9:
                    self.reduce_surrounding_cells(x, y)
        if recursive and total_entropy_old != self.get_total_entropy():
            self.reduce_map(recursive=True)
        print('\t end reduce map')

    def is_resolved(self, x, y):
        return self.get_entropy(x, y) == 1

    def print_entropy_stats(self):  # TODO: make the return value into a dictionary
        total = 0
        lowest = len(self.variations)
        highest = 1
        num_cells = self.width * self.height
        for x in range(self.width):
            for y in range(self.height):
                e = self.get_entropy(x, y)
                total += e
                if e > highest:
                    highest = e
                if e < lowest:
                    lowest = e
        average = total / num_cells

        print(f'Map Entropy: {total} over {num_cells} from {lowest} to {highest} averaging {average}')

    def get_total_entropy(self) -> int:
        """
        The sum of all entropy values of all cells
        :return:
        """
        total = 0
        for x in range(self.width):
            for y in range(self.height):
                e = self.get_entropy(x, y)
                total += e
        return total

    def num_variations(self):
        """
        Number of variations
        :return:
        """
        return len(self.variations)

    def resolve_step(self):
        """
        Tries to find the lowest cell and resolve it or something
        :return:
        """
        self.reduce_map(recursive=True)
        candidates = list()
        lowest = self.num_variations() + 1
        for x in range(self.width):
            for y in range(self.height):
                e = self.get_entropy(x, y)
                if lowest > e > 1:
                    lowest = e
                    candidates.append({'x': x, 'y': y, 'entropy': e, 'sur_sum': self.get_surrounding_sum(x, y)})
        print('Potential candidates: ', len(candidates))
        pprint(candidates)
        for c in candidates[:]:  # make a copy of the list
            if c['entropy'] > lowest:
                candidates.remove(c)
        print('Remaining candidates: ', len(candidates))
        final_candidates = sorted(candidates, key=itemgetter('sur_sum'))
        pprint(final_candidates)
        winner = final_candidates[0]
        print('Winning cell: ', winner)

        self.resolve_cell_randomly(winner['x'], winner['y'])
        self.reduce_surrounding_cells(winner['x'], winner['y'])

    def resolve_step_fast(self):
        """
        Faster but less accurate version of resolve_step
        :return:
        """
        self.reduce_map(recursive=True)
        candidates = list()
        lowest = self.num_variations() + 1
        for x in range(self.width):
            for y in range(self.height):
                e = self.get_entropy(x, y)
                if lowest > e > 1:
                    lowest = e
                    candidates.append({'x': x, 'y': y, 'entropy': e, 'sur_sum': self.get_surrounding_sum(x, y)})
        for c in candidates[:]:  # make a copy of the list
            if c['entropy'] > lowest:
                candidates.remove(c)
        final_candidates = sorted(candidates, key=itemgetter('sur_sum'))
        if len(final_candidates) == 0:
            return False
        else:
            winner = final_candidates[0]
            self.resolve_cell_randomly(winner['x'], winner['y'])
            return True

    def get_surrounding_sum(self, x, y):
        """
        Sum of all entropies in a 3x3 grid with the x/y at the center. This includes the center cell's value.
        :param x:
        :param y:
        :return:
        """
        r = 0
        for xx in range(x - 1, x + 2):
            for yy in range(y - 1, y + 2):
                if self.width > xx >= 0 and self.height > yy >= 0:
                    r += self.get_entropy(xx, yy)
                else:
                    r += self.num_variations() + 1
        return r

    # TODO: add __repr__()
