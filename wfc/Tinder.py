import os

from PIL import Image


class Tinder:
    """
    This class can generate and a look-up table for a folder with quadratic images to check for matches.
    """
    def __init__(self, folder: str):
        self.folder = folder
        self.file_list = os.listdir(folder)

    def get_pixel_edges(self, image_path: str):
        pil_image = Image.open(image_path)
        pixel_image = pil_image.load()
        width, height = pil_image.size
        assert width == height, "Image is not quadratic: " + image_path
        # TODO: continue here
        # return all 4 edges as 1-pixel-wide arrays


