import os
from typing import List

import arcade

from wfc.Tile import Tile
from wfc.TimingEvent import TimingEvent
from wfc.WFCMap import WFCMap

# Options to customize:
# HOWTO: right-click a cell to solve it randomly and then watch it solve automatically
# TILESET
# - basictiles2
#   edge = 20
#   sockets = 9
#
tile_set_folder_name = 'simplecolors'
tile_set_edge_length = 20 # TODO: auto generate this value from the first image in the folder?
tiles_x = 20
sockets_per_edge = 1
WINDOW_WIDTH = 1000
STEP_SLEEP = 2

# Only touch these options, if you know what you are doing:
tiles_y = tiles_x
WINDOW_HEIGHT = WINDOW_WIDTH
TILE_SCALING = WINDOW_WIDTH / tiles_x / tile_set_edge_length
WINDOW_BACKGROUND_COLOR = arcade.color.BLACK


class WFCMapGenerator(arcade.Window):
    def __init__(self):
        super().__init__(WINDOW_WIDTH, WINDOW_HEIGHT, "Fun With Wave Function Collapse", resizable=False)

        arcade.set_background_color(WINDOW_BACKGROUND_COLOR)

        self.ever_second_timer = TimingEvent(timeout=STEP_SLEEP)
        self.stepper_active = False

        self.displayed_sprites = arcade.SpriteList()  # All sprites that are actually rendered go here

        # Generate Tile objects from all the image files in tile_set_path
        self.tile_set: List[Tile] = list()
        full_asset_path = os.path.join('..', 'assets', tile_set_folder_name)
        for filename in os.listdir(full_asset_path):
            new_tile = Tile(os.path.join(full_asset_path, filename), sockets_per_edge=sockets_per_edge)
            self.tile_set.append(new_tile)
            # TODO: create a function which makes rotated versions of images

        print(f'number of tiles including rotations: {len(self.tile_set)}')
        for index, t in enumerate(self.tile_set):
            print(f'{index}\t {t}')

        # Initialize the underlying entropy map
        self.wfc_map = WFCMap(tiles_x, tiles_y, variations=self.tile_set, init_grid=True)

    def on_mouse_press(self, x, y, button, modifiers):
        col = int(x // tiles_x // TILE_SCALING)
        row = int((WINDOW_HEIGHT - y) // tiles_y // TILE_SCALING)
        print(f'Mouse click at [{col}][{row}]')

        if button == 4:  # Resolve cell under mouse randomly
            self.stepper_active = True
            if self.wfc_map.resolve_cell_randomly(col, row):
                self.wfc_map.reduce_surrounding_cells(col, row)
                self.rebuild_sprite_grid()
        else:
            print(self.wfc_map.grid[col][row])
            self.wfc_map.resolve_step_fast()
            # self.wfc_map.resolve_step()
            self.rebuild_sprite_grid()

    def on_update(self, delta_time: float):
        if self.ever_second_timer.update_and_fire_check(time_delta=delta_time) and self.stepper_active:
            if not self.wfc_map.resolve_step_fast():
                self.stepper_active = False
            self.rebuild_sprite_grid()

    def on_draw(self):
        self.clear()
        self.draw_grid()
        self.displayed_sprites.draw()

    def rebuild_sprite_grid(self):
        print(f'Rebuilding displayed sprite list')
        self.displayed_sprites = arcade.SpriteList()
        for x in range(self.wfc_map.width):
            for y in range(self.wfc_map.height):
                if self.wfc_map.is_resolved(x, y):
                    self.add_sprite_to_grid(col=x, row=y, tile=self.wfc_map.grid[x][y])
        print(f'Done rebuilding sprite grid. New size: {len(self.displayed_sprites)}')

    def add_sprite_to_grid(self, col: int, row: int, tile: Tile):
        if type(tile) == list:
            tile = tile[0]
        new_sprite = arcade.Sprite(tile.image_path, TILE_SCALING)
        new_sprite.center_x = WINDOW_WIDTH / tiles_x * col + WINDOW_WIDTH / tiles_x / 2
        new_sprite.center_y = WINDOW_HEIGHT / tiles_y * (tiles_y - row - 1) + WINDOW_HEIGHT / tiles_y / 2
        self.displayed_sprites.append(new_sprite)

    def draw_grid(self):
        for row in range(0, tiles_y):
            for col in range(0, tiles_x):
                x = WINDOW_WIDTH / tiles_x * col + WINDOW_WIDTH / tiles_x / 2
                y = WINDOW_HEIGHT / tiles_y * (tiles_y - row - 1) + WINDOW_HEIGHT / tiles_y / 2
                arcade.draw_rectangle_filled(x, y, WINDOW_WIDTH / tiles_x - 1, WINDOW_HEIGHT / tiles_y - 1, arcade.color.ORANGE)
                arcade.draw_text(start_x=x - WINDOW_WIDTH / tiles_x / 2 + 5, start_y=y, text=self.wfc_map.get_entropy(col, row), color=arcade.color.DARK_BLUE, font_size=round(20-self.wfc_map.get_entropy(col, row)))

    def draw_text_to_grid(self, col: int, row: int, text: str, color: arcade.color = arcade.color.CYAN):
        """
        Draws a text to grid position x = col and y = row.
        :param col:
        :param row:
        :param text:
        :param color: optional arcade.color
        :return:
        """
        x = WINDOW_WIDTH / tiles_x * col + WINDOW_WIDTH / tiles_x / 2
        y = WINDOW_HEIGHT / tiles_y * (tiles_y - row - 1) + WINDOW_HEIGHT / tiles_y / 2
        arcade.draw_text(start_x=x - WINDOW_WIDTH / tiles_x / 2, start_y=y, text=text, color=color)


if __name__ == '__main__':
    print("Starting program and initiating game")
    window = WFCMapGenerator()
    arcade.run()
