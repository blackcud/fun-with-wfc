import os
import unittest

from wfc.Tile import Tile


class TestTile(unittest.TestCase):
    def setUp(self) -> None:
        self.tile_path = os.path.join('..', 'assets', 'unittests', 'unittest.png')
        self.tile_path2 = os.path.join('..', 'assets', 'unittests', 'unittest_2.png')
        self.tile_path4 = os.path.join('..', 'assets', 'unittests', 'unittest_4.png')
        self.sockets_per_edge = 3
        self.color_nw = (0, 0, 0)  # black
        self.color_n = (255, 0, 0)  # red
        self.color_ne = (255, 255, 0)  # yellow
        self.color_e = (0, 255, 0)  # green
        self.color_se = (0, 255, 255)  # cyan
        self.color_s = (0, 0, 255)  # blue
        self.color_sw = (100, 100, 100)  # grey
        self.color_w = (200, 100, 50)  # brown
        self.north_socket = {0: self.color_nw, 1: self.color_n, 2: self.color_ne}
        self.east_socket = {0: self.color_ne, 1: self.color_e, 2: self.color_se}
        self.south_socket = {0: self.color_se, 1: self.color_s, 2: self.color_sw}
        self.west_socket = {0: self.color_sw, 1: self.color_w, 2: self.color_nw}

    def test_unittest_png_is_in_order(self):
        self.tt = Tile(self.tile_path, sockets_per_edge=self.sockets_per_edge)
        self.assertEqual(self.tt.width, 100)
        self.assertEqual(self.tt.height, 100)

    def test_generate_sockets(self):
        self.tt = Tile(self.tile_path, sockets_per_edge=self.sockets_per_edge)
        self.assertEqual(len(self.tt.sockets), 4)
        for d in Tile.sides:
            self.assertEqual(len(self.tt.sockets[d]), self.sockets_per_edge)
        self.assertEqual(self.north_socket, self.tt.sockets['north'])
        self.assertEqual(self.east_socket, self.tt.sockets['east'])
        self.assertEqual(self.south_socket, self.tt.sockets['south'])
        self.assertEqual(self.west_socket, self.tt.sockets['west'])

    def test_is_socket_fit(self):
        self.tt = Tile(self.tile_path, sockets_per_edge=self.sockets_per_edge)
        self.assertTrue(self.tt.is_socket_fit(self.north_socket, side='north'))
        self.assertFalse(self.tt.is_socket_fit(self.east_socket, side='north'))

    def test_is_socket_fit_v2(self):
        self.south_tile = Tile(self.tile_path2, sockets_per_edge=3)
        self.north_tile = Tile(self.tile_path4, sockets_per_edge=3)
        self.assertTrue(self.south_tile.is_socket_fit(self.north_tile.sockets['south'], 'north'))
        self.assertTrue(self.south_tile.is_socket_fit(self.north_tile.sockets['west'], 'east'))
        self.assertFalse(self.south_tile.is_socket_fit(self.north_tile.sockets['west'], 'south'))

    def test_rotational_copy(self):
        self.tt = Tile(self.tile_path, sockets_per_edge=self.sockets_per_edge)
        self.rt = self.tt.create_rotational_copy()
        self.assertNotEqual(self.tt, self.rt)
        self.assertEqual(self.tt.image_path, self.rt.image_path)
        self.assertEqual(self.tt.width, self.rt.width)
        self.assertEqual(self.tt.sockets_per_edge, self.rt.sockets_per_edge)
        self.assertEqual(self.tt.sockets['north'], self.rt.sockets['east'])
        self.assertEqual(self.tt.sockets['east'], self.rt.sockets['south'])
        self.assertEqual(self.tt.sockets['south'], self.rt.sockets['west'])
        self.assertEqual(self.tt.sockets['west'], self.rt.sockets['north'])


if __name__ == '__main__':
    unittest.main()
