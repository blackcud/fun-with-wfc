import os
import unittest

from wfc.Tile import Tile
from wfc.WFCMap import WFCMap


class TestWFCMap(unittest.TestCase):
    def setUp(self) -> None:
        self.sockets_per_edge = 3
        self.width = 10
        self.height = 10

        self.tile_path = os.path.join('..', 'assets', 'unittests', 'unittest.png')
        self.tile_path2 = os.path.join('..', 'assets', 'unittests', 'unittest_2.png')
        self.tile_path3 = os.path.join('..', 'assets', 'unittests', 'unittest_3.png')
        self.tile_path4 = os.path.join('..', 'assets', 'unittests', 'unittest_4.png')

        self.tile1 = Tile(self.tile_path, sockets_per_edge=self.sockets_per_edge)
        self.tile2 = self.tile1.create_rotational_copy()
        self.tile3 = self.tile2.create_rotational_copy()
        self.tile4 = Tile(self.tile_path2, sockets_per_edge=self.sockets_per_edge)
        self.tile5 = Tile(self.tile_path3, sockets_per_edge=self.sockets_per_edge)
        self.tile6 = Tile(self.tile_path4, sockets_per_edge=self.sockets_per_edge)
        self.variations = [self.tile1, self.tile4, self.tile5, self.tile6]
        self.num_variations = len(self.variations)
        self.wfc_map = WFCMap(height=self.height, width=self.width, variations=self.variations, init_grid=True)
        for x in range(self.width):
            for y in range(self.height):
                self.assertEqual(len(self.variations), len(self.wfc_map.grid[x][y]))

    def tearDown(self) -> None:
        self.wfc_map = None

    def test_reduce_north(self):
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(self.wfc_map.get_entropy(1, 0), self.num_variations)
        self.assertTrue(self.wfc_map.reduce_variations_north_of_position(1, 1))
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(self.wfc_map.get_entropy(1, 0), 1)

    def test_reduce_south(self):
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(self.wfc_map.get_entropy(1, 0), self.num_variations)
        self.assertTrue(self.wfc_map.reduce_variations_south_of_position(1, 0))
        self.assertEqual(self.wfc_map.get_entropy(1, 0), self.num_variations)
        self.assertEqual(2, self.wfc_map.get_entropy(1, 1))

    def test_reduce_east(self):
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(self.wfc_map.get_entropy(2, 1), self.num_variations)
        self.assertTrue(self.wfc_map.reduce_variations_east_of_position(1, 1))
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(3, self.wfc_map.get_entropy(2, 1))

    def test_reduce_west(self):
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(self.wfc_map.get_entropy(0, 1), self.num_variations)
        self.assertTrue(self.wfc_map.reduce_variations_west_of_position(1, 1))
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        self.assertEqual(1, self.wfc_map.get_entropy(0, 1))

    def test_resolved_cells_counters(self):
        self.assertEqual(0, self.wfc_map.get_number_of_resolved_cells())
        self.wfc_map.grid[0][0] = self.tile1
        self.assertEqual(1, self.wfc_map.get_number_of_resolved_cells())
        self.wfc_map.grid[1][2] = self.tile2
        self.assertEqual(2, self.wfc_map.get_number_of_resolved_cells())
        self.assertTrue(self.wfc_map.is_resolved(1, 2))
        self.assertTrue(self.wfc_map.is_resolved(0, 0))

    def test_resolved_cell_randomly(self):
        self.assertEqual(0, self.wfc_map.get_number_of_resolved_cells())
        self.assertEqual(self.wfc_map.get_entropy(1, 1), self.num_variations)
        i = 0
        for x in range(self.width):
            for y in range(self.height):
                self.assertTrue(self.wfc_map.resolve_cell_randomly(x, y))
                i += 1
                self.assertEqual(i, self.wfc_map.get_number_of_resolved_cells())

    # TODO more wfc_map unittests


if __name__ == '__main__':
    unittest.main()
